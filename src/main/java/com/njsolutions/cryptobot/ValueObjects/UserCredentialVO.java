/*
 * Copyright (c) 2017. NjSolutions
 */

package com.njsolutions.cryptobot.ValueObjects;

import eu.verdelhan.ta4j.Decimal;

public class UserCredentialVO {

    private Decimal balance;
    private String apiKey;
    private String apiSeceret;

    public Decimal getBalance() {
        return balance;
    }

    public void setBalance(Decimal balance) {
        this.balance = balance;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiSeceret() {
        return apiSeceret;
    }

    public void setApiSeceret(String apiSeceret) {
        this.apiSeceret = apiSeceret;
    }

}
