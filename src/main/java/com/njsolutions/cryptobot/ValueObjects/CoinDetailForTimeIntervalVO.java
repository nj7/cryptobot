/*
 * Copyright (c) 2017. NjSolutions
 */

package com.njsolutions.cryptobot.ValueObjects;

import eu.verdelhan.ta4j.Decimal;
import eu.verdelhan.ta4j.Tick;
import org.joda.time.DateTime;
import org.joda.time.Period;

import java.time.ZonedDateTime;

public class CoinDetailForTimeIntervalVO extends Tick {


    public CoinDetailForTimeIntervalVO(Period timePeriod, DateTime endTime) {
        super(timePeriod, endTime);
    }

    public CoinDetailForTimeIntervalVO(DateTime endTime, double openPrice, double highPrice, double lowPrice, double closePrice, double volume) {
        super(endTime, openPrice, highPrice, lowPrice, closePrice, volume);
    }

    public CoinDetailForTimeIntervalVO(DateTime endTime, String openPrice, String highPrice, String lowPrice, String closePrice, String volume) {
        super(endTime, openPrice, highPrice, lowPrice, closePrice, volume);
    }

    public CoinDetailForTimeIntervalVO(DateTime endTime, Decimal openPrice, Decimal highPrice, Decimal lowPrice, Decimal closePrice, Decimal volume) {
        super(endTime, openPrice, highPrice, lowPrice, closePrice, volume);
    }

    public CoinDetailForTimeIntervalVO(Period timePeriod, DateTime endTime, Decimal openPrice, Decimal highPrice, Decimal lowPrice, Decimal closePrice, Decimal volume) {
        super(timePeriod, endTime, openPrice, highPrice, lowPrice, closePrice, volume);
    }

}