package com.njsolutions.cryptobot;

import com.njsolutions.cryptobot.BittrexApi.GetCurrencyData;
import com.njsolutions.cryptobot.Bot.AltCoinBot;
import com.njsolutions.cryptobot.CSVFileWriter.WriteTradingRecord;
import com.njsolutions.cryptobot.ValueObjects.UserCredentialVO;
import eu.verdelhan.ta4j.*;

import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.util.*;

public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "............................Initialising Trading Bot copyright njSolutions............................\n" );

        List<String> altCoinList = new GetCurrencyData("","").getALtcoinsListFromBittrex();
         /*       new ArrayList<>();

        altCoinList.add("ADT");
        altCoinList.add("ANT");
        altCoinList.add("ARK");
        altCoinList.add("BSD");
        altCoinList.add("EDG");
        altCoinList.add("ION");
        altCoinList.add("MAID");
        altCoinList.add("MEME");
        altCoinList.add("PIVX");
        altCoinList.add("REP");
        altCoinList.add("RISE");
        altCoinList.add("SBD");
        altCoinList.add("VOX");
        altCoinList.add("VRC");
        altCoinList.add("XCP");
        altCoinList.add("ZCL");*/

        UserCredentialVO userVO = new UserCredentialVO();
        userVO.setBalance(Decimal.valueOf(0.03));
        userVO.setApiKey("notSet");
        userVO.setApiSeceret("notSet");
        Map<String,AltCoinBot> altCoinBots = new LinkedHashMap<>();
        HashMap<String,Integer> buyiedOrder = new LinkedHashMap<>();


        WriteTradingRecord write = null;
        try {
            write = new WriteTradingRecord();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        for (String s: altCoinList ) {
            /*
             * Time frames for bittrex are
             * oneMin
             * fiveMin
             * thirtyMin
             * day
             */
            AltCoinBot altCoinBot = new AltCoinBot(userVO, "BTC-"+s, "thirtyMin",write);
            altCoinBots.putIfAbsent(s,altCoinBot);
        }
        int ind = 1;
        int reupdateToken = 1;
        System.out.println( "\n\n..............................Starting Trading Bot copyright njSolutions..............................\n\n" );
        while (true) {

            for (Map.Entry<String, AltCoinBot> entry : altCoinBots.entrySet()) {
                String altcoin = entry.getKey();
                AltCoinBot bot = entry.getValue();
                if (userVO.getBalance().isGreaterThan(Decimal.valueOf(0.00099)) ||
                        (userVO.getBalance().isLessThan(Decimal.valueOf(0.001)) && !buyiedOrder.isEmpty() && buyiedOrder.containsKey(altcoin))) {

                    System.out.println("Iterating for : " + altcoin + " at : " + LocalDateTime.now());
                    bot.getDataFromBittrex();
                    int i = bot.runStrategy();
                    if (i == 1) {
                        buyiedOrder.put(altcoin, 1);
                    } else if (i == 0) {
                        buyiedOrder.remove(altcoin);
                    }
                } else if (userVO.getBalance().isLessThan(Decimal.valueOf(0.001)) && buyiedOrder.isEmpty()) {
                    System.out.println("The Bot is not correct");
                    System.exit(0);
                }
            }
            System.out.println("\n\n\n");
            if(ind++%10 ==0){
                System.out.println( "...........................Balance Information..........................." );
                System.out.println("The Current Balance is : "+userVO.getBalance()+" with "+buyiedOrder.size()+" order placed of each value 0.001\n\n\n");
            }
        }
    }
}
