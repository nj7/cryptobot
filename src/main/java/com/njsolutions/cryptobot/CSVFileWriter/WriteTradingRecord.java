/*
 * Copyright (c) 2017. NjSolutions
 */

package com.njsolutions.cryptobot.CSVFileWriter;

import com.njsolutions.cryptobot.ValueObjects.TradingOrderVO;

import java.io.*;
import java.time.LocalDateTime;
import java.util.List;

public class WriteTradingRecord {

    //writing trading record to csv file
    //with columns ID,Quantity,BuyingPrice,SellingPrice,ProfitORLoss,totalProfitORtotalLoss,ProfitOrLossPercentage
    private File file;
    private StringBuilder sb;
    private int i;

    public WriteTradingRecord() throws FileNotFoundException {
        this.file = new File(  "target/Trades.csv");
        this.sb = new StringBuilder();
        this.i = 1;
        initializeFile();
    }

    private void initializeFile() throws FileNotFoundException {
        PrintWriter pw = new PrintWriter(file);
        sb.append("ID");
        sb.append(",");
        sb.append("AltCoinName");
        sb.append(',');
        sb.append("Quantity");
        sb.append(',');
        sb.append("Buying Price");
        sb.append(',');
        sb.append("Selling Price");
        sb.append(',');
        sb.append("Profit OR Loss");
        sb.append(',');
        sb.append("Total Change");
        sb.append(',');
        sb.append("Total Change Percentage");
        sb.append('\n');
        pw.append(sb.toString());
        sb = null;
        pw.close();
    }

    public void writeRecords(TradingOrderVO order,String altCoinName) throws FileNotFoundException {

        PrintWriter pw = new PrintWriter(new FileOutputStream(file,true));
        sb = new StringBuilder();
        sb.append(i++);
        sb.append(',');
        sb.append(altCoinName);
        sb.append(',');
        sb.append(order.getQuantity());
        sb.append(',');
        sb.append(order.getBuyingPrice());
        sb.append(',');
        sb.append(order.getSellingPrice());
        sb.append(',');
        if (order.getSellingPrice() != null){
            sb.append(order.getSellingPrice().isGreaterThan(order.getBuyingPrice()) ? "Profit" : "Loss");
            sb.append(',');
        }
        sb.append(order.getChange());
        sb.append(',');
        sb.append(order.getChangePercentage());
        sb.append('\n');

        pw.append(sb.toString());
        pw.close();
        sb = null;

        System.out.println("File for : " + altCoinName + " written at : " + LocalDateTime.now());
    }
}
