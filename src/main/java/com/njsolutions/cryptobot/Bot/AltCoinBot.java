/*
 * Copyright (c) 2017. NjSolutions
 */

package com.njsolutions.cryptobot.Bot;

import com.njsolutions.cryptobot.BittrexApi.GetCurrencyData;
import com.njsolutions.cryptobot.CSVFileWriter.WriteTradingRecord;
import com.njsolutions.cryptobot.Strategies.StrategyJ;
import com.njsolutions.cryptobot.ValueObjects.CoinDetailTimeSeries;
import com.njsolutions.cryptobot.ValueObjects.TradingOrderVO;
import com.njsolutions.cryptobot.ValueObjects.UserCredentialVO;
import eu.verdelhan.ta4j.Decimal;
import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TimeSeries;

import java.io.FileNotFoundException;
import java.util.List;

public class AltCoinBot {

    private String altCoinName;
    private String timeFrame;
    private UserCredentialVO userVo;
    private TimeSeries altCoinTimeSeries;
    private int ticksCount;
    private TradingOrderVO order;
    private WriteTradingRecord write;

    public AltCoinBot(UserCredentialVO userVo,String altCoinName, String timeFrame,WriteTradingRecord write){
        this.userVo = userVo;
        this.timeFrame =timeFrame;
        this.altCoinName = altCoinName;
        this.order = new TradingOrderVO();
        this.write = write;
    }
    public void getDataFromBittrex(){

        GetCurrencyData getCurrencyData = new GetCurrencyData(altCoinName,timeFrame);
        List<Tick> altCoinHistory = getCurrencyData.getCoinPriceHistory();

        TimeSeries altCoinTimeSeries = new CoinDetailTimeSeries(altCoinHistory);

        this.altCoinTimeSeries = altCoinTimeSeries;

    }


    public int runStrategy(){
        ticksCount = altCoinTimeSeries.getTickCount()-1;
        if(ticksCount < 0)
            return -1;
        StrategyJ strategy = new StrategyJ(altCoinTimeSeries);

        if(strategy.buyingRule() && order.getBuyingPrice() == null && userVo.getBalance().isGreaterThan(Decimal.valueOf(0.00099))){
            placeBuyOrder();
            return 1;
        }
        else if(strategy.sellingRule() && order.getBuyingPrice() != null && order.getSellingPrice() == null
                && (altCoinTimeSeries.getTick(ticksCount).getClosePrice().dividedBy(order.getBuyingPrice()).isGreaterThan(Decimal.valueOf(1.007)))){
            placeSellOrder();
            return 0;
        }
        return -1;
    }

    private void placeBuyOrder(){
        order.setBuyingPrice(altCoinTimeSeries.getTick(ticksCount).getClosePrice());
        order.setQuantity(Decimal.valueOf(0.001).dividedBy(order.getBuyingPrice()));
        userVo.setBalance(userVo.getBalance().minus(Decimal.valueOf(0.001)));
        System.out.println("Buyied : "+altCoinName+" at : "+order.getBuyingPrice() );
    }

    private void placeSellOrder(){
        order.setSellingPrice(altCoinTimeSeries.getTick(ticksCount).getClosePrice());
        order.setChange(order.getQuantity().multipliedBy(order.getSellingPrice().minus(order.getBuyingPrice())));
        order.setChangePercentage((order.getChange().dividedBy(order.getBuyingPrice().multipliedBy(order.getQuantity()))).multipliedBy(Decimal.valueOf(100)));
        System.out.println("Selling : "+altCoinName+" at : "+order.getSellingPrice() );
        userVo.setBalance(userVo.getBalance().plus(order.getQuantity().multipliedBy(order.getSellingPrice())));
        try {
            write.writeRecords(order,altCoinName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        order = null;
        order = new TradingOrderVO();
    }

}
