/*
 * Copyright (c) 2017. NjSolutions
 */

package com.njsolutions.cryptobot.Strategies;

import eu.verdelhan.ta4j.Decimal;
import eu.verdelhan.ta4j.Rule;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.indicators.oscillators.*;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import eu.verdelhan.ta4j.indicators.trackers.SMAIndicator;
import eu.verdelhan.ta4j.trading.rules.OverIndicatorRule;
import eu.verdelhan.ta4j.trading.rules.UnderIndicatorRule;

public class StrategyJ {
    /*
     * this the first strategy devised by Nirmit Jain
     * in this strategyJ  buy signal are fired when
     * trend line crossover are seen using sma 9 on selected coins on five min chart
     * agrees for tie interval of 30 min
     */
    private StochasticOscillatorKIndicator stochK;
    private StochasticOscillatorDIndicator stochD;
    private ClosePriceIndicator closePriceIndicator;
    private CMOIndicator cmo;
    private CCIIndicator cci;
    private int totalElements;


    public StrategyJ(TimeSeries timeSeries){
        this.totalElements = timeSeries.getTickCount();
        stochK = new StochasticOscillatorKIndicator(timeSeries,14);
        stochD = new StochasticOscillatorDIndicator(stochK);
        closePriceIndicator = new ClosePriceIndicator(timeSeries);
        cmo = new CMOIndicator(closePriceIndicator,9);
        cci = new CCIIndicator(timeSeries,20);
    }
    public boolean buyingRule(){

        boolean toBuy = stochD.getValue(totalElements-1).isLessThan(stochK.getValue(totalElements-1)) &&
                cci.getValue(totalElements-1).isGreaterThan(cci.getValue(totalElements-2))&&
                stochK.getValue(totalElements-1).isGreaterThan(stochK.getValue(totalElements-2))&&
                cmo.getValue(totalElements-1).isGreaterThan(cmo.getValue(totalElements-2))&&
                stochD.getValue(totalElements-3).isGreaterThan(stochK.getValue(totalElements-3));


        Rule buyingRule = new OverIndicatorRule(stochK,stochD)
                .and(new UnderIndicatorRule(cci, Decimal.valueOf(0)))
                .and(new UnderIndicatorRule(cmo,Decimal.valueOf(0)))
                .and(new UnderIndicatorRule(stochK,Decimal.valueOf(50)));

        System.out.println("\nIs satisified to buy : "+ (toBuy && buyingRule.isSatisfied(totalElements-2)));

        return buyingRule.isSatisfied(totalElements-2) && toBuy ;

    }

    public boolean sellingRule(){

        Rule sellingRule = new OverIndicatorRule(stochD,stochK)
                .and(new UnderIndicatorRule(cci, cci.getValue(totalElements-2)))
                .and(new UnderIndicatorRule(cmo,cmo.getValue(totalElements-2)));

        boolean toSell = sellingRule.isSatisfied(totalElements-1);

        System.out.println("Is satisified to Sell : "+ toSell +"\n\n");

        return toSell;

    }
}
