package com.njsolutions.cryptobot.AltCoinForeCast;

import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;

public class AltCoinForeCast {
    org.jsoup.nodes.Document document;
    LinkedHashMap<String,String> prominentMap;
    TreeMap<Double,String> prioritizedMap;
    int loopingVar;

    public AltCoinForeCast(){
        prominentMap = new LinkedHashMap<>();
        prioritizedMap = new TreeMap<>();
        loopingVar = 1;
        addDatatoList();
    }
    public void addDatatoList(){

        prominentMap.put("adex","ADX");
        prominentMap.put("ark","ARK");
        prominentMap.put("aragon","ANT");
        prominentMap.put("artbyte","ABY");
        prominentMap.put("augur","REP");
        prominentMap.put("bitbay","BAY");
        prominentMap.put("bitcoincash","BCC");
        prominentMap.put("bitcoindark","BTCD");
        prominentMap.put("bitcoingold","BTG");
        prominentMap.put("civic","CVC");
        prominentMap.put("counterparty","XCP");
        prominentMap.put("dash","DASH");
        prominentMap.put("decred","DCR");
        prominentMap.put("digibyte","DGB");
        prominentMap.put("ethereum","ETH");
        prominentMap.put("ethereumclassic","ETC");
        prominentMap.put("factom","FCT");
        prominentMap.put("gamecredits","GAME");
        prominentMap.put("golem","GNT");
        prominentMap.put("iexec","RLC");
        prominentMap.put("iocoin","IOC");
        prominentMap.put("komodo","KMD");
        prominentMap.put("lisk","LSK");
        prominentMap.put("litecoin","LTC");
        prominentMap.put("monero","XMR");
        prominentMap.put("maidsafecoin","MAID");
        prominentMap.put("navcoin","NAV");
        prominentMap.put("nem","XEM");
        prominentMap.put("neo","NEO");
        prominentMap.put("neoscoin","NEOS");
        prominentMap.put("nexium","NXC");
        prominentMap.put("nxtcoin","NXT");
        prominentMap.put("omisego","OMG");
        prominentMap.put("peercoin","PPC");
        prominentMap.put("potcoin","POT");
        prominentMap.put("qtum","QTUM");
        prominentMap.put("ripple","XRP");
        prominentMap.put("siacoin","SC");
        prominentMap.put("statussnt","SNT");
        prominentMap.put("steem","STEEM");
        prominentMap.put("stellar","XLM");
        prominentMap.put("stratis","STRAT");
        prominentMap.put("synero","AMP");
        prominentMap.put("tenx","PAY");
        prominentMap.put("ubiq","UBQ");
        prominentMap.put("verge","XVG");
        prominentMap.put("waves","WAVES");
        prominentMap.put("zcash","ZEC");

    }
    //Html Scrapper for altcoin forecast

    public void generatePercentage(){
        for (Map.Entry entry : prominentMap.entrySet()){

            try {
                document = Jsoup.connect("http://www.altcoinforecast.com/"+entry.getKey()).get();
            } catch (IOException e) {
                try {
                    document = Jsoup.connect("http://www.altcoinforecast.com/"+entry.getKey()).get();
                } catch (IOException e1) {
                    System.out.print("error for "+entry.getKey());
                }
            }
            if(document !=null){
                Elements elements = document.getElementsByClass("kpi");
                Double shortGoal = Double.valueOf(elements.get(0).text());
                Double current = Double.valueOf(elements.get(2).text());
                shortGoal = shortGoal - current;
                if(current > 0.00000049)
                    prioritizedMap.put(shortGoal/current, (String) entry.getValue());

                System.out.print("* ");
            }
            document = null;
        }
    }

    public List getPrioritizedList(){
        List<String> list = new ArrayList<>();
        for (Map.Entry entry : prioritizedMap.entrySet()){
            list.add((String) entry.getValue());
        }
        Collections.reverse(list);
        return list;
    }

}
