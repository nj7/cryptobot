/*
 * Copyright (c) 2017. NjSolutions
 */

package com.njsolutions.cryptobot.ValueObjects;

import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TimeSeries;
import org.joda.time.Period;

import java.util.List;

public class CoinDetailTimeSeries extends TimeSeries {
    public CoinDetailTimeSeries(String name, List<Tick> ticks) {
        super(name, ticks);
    }

    public CoinDetailTimeSeries(List<Tick> ticks) {
        super(ticks);
    }

    public CoinDetailTimeSeries(String name, Period timePeriod) {
        super(name, timePeriod);
    }

    public CoinDetailTimeSeries(Period timePeriod) {
        super(timePeriod);
    }
}
