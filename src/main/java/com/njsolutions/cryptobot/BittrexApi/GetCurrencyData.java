/*
 * Copyright (c) 2017. NjSolutions
 */

package com.njsolutions.cryptobot.BittrexApi;

import com.njsolutions.cryptobot.URLJSONReader.ReadJSONFROMURL;
import com.njsolutions.cryptobot.ValueObjects.CoinDetailForTimeIntervalVO;
import eu.verdelhan.ta4j.Decimal;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;
import org.mortbay.util.ajax.JSON;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class GetCurrencyData {
    /*
    * this class fetch the live data from the
    * bittrex platform
    * using the following
    * pair format BTC-BAY
    * api = https://bittrex.com/Api/v2.0/pub/market/GetTicks?marketName={pair}&tickInterval=thirtyMin
     */
    private List<CoinDetailForTimeIntervalVO> coinDetailForTimeIntervalVOS;
    private String pair;
    private String timeInterval;

    public GetCurrencyData(String pair, String timeInterval){

        this.pair = pair;
        this.timeInterval = timeInterval;

        if(!pair.equals(""))
            getCoinHistory();

    }

    public void getCoinHistory(){
        ReadJSONFROMURL readJSONFROMURL = new ReadJSONFROMURL();
        JSONObject jsonObject = null;
        List<CoinDetailForTimeIntervalVO> coinTimeLine = new LinkedList<CoinDetailForTimeIntervalVO>();
        try {
            jsonObject = readJSONFROMURL.readUrl("https://bittrex.com/Api/v2.0/pub/market/GetTicks?marketName="
                    +pair+"&tickInterval="+timeInterval);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!jsonObject.equals(null) && !jsonObject.get("result").equals(null)){
            JSONArray jsonArray ;
            jsonArray = (JSONArray) jsonObject.get("result");
            for (Object j : jsonArray) {

                JSONObject detail = (JSONObject) j;

                //changing date from json to ZonedDateTime
                String date = (String) detail.get("T");
                date = date.replace("T"," ");
                DateTime dt = DateTime.parse(date, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));

                CoinDetailForTimeIntervalVO tempObject = new CoinDetailForTimeIntervalVO(dt,
                        (Double) detail.get("O"), (Double) detail.get("H"), (Double) detail.get("L"),
                        (Double) detail.get("C"), (Double) detail.get("V"));

                coinTimeLine.add(tempObject);
            }
        }
        this.coinDetailForTimeIntervalVOS = coinTimeLine;
    }

    public List getCoinPriceHistory(){
        return coinDetailForTimeIntervalVOS;
    }

    public List getALtcoinsListFromBittrex(){

        JSONObject jsonObject = null;
        List<String > altCoinList = new ArrayList<>();

        try {
            jsonObject = new ReadJSONFROMURL().readUrl("https://www.bittrex.com/api/v2.0/pub/Markets/GetMarketSummaries?_=1513007015414");
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(jsonObject != null){
            JSONArray jsonArray;
            jsonArray = (JSONArray) jsonObject.get("result");
            String name;
            for (Object j : jsonArray) {

                JSONObject detail = (JSONObject) j;
                JSONObject noticeObject = (JSONObject) j;
                noticeObject = (JSONObject) noticeObject.get("Market");
                // not taking coins that are going to be delisted
                boolean notice = false;
                if (!noticeObject.get("Notice").equals(null))
                    notice = noticeObject.get("Notice").toString().contains("delisted");

                detail = (JSONObject) detail.get("Summary");
                Double volume = (Double) detail.get("BaseVolume");
                name = (String) detail.get("MarketName");

                if(name.split("-")[0].equals("BTC") && volume > 100 && !notice){
                    //System.out.println(name);
                    name = name.split("-")[1];
                    altCoinList.add(name);
                }
            }
        }

        return altCoinList;
    }
}
