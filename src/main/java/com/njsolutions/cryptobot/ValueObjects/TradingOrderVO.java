/*
 * Copyright (c) 2017. NjSolutions
 */

package com.njsolutions.cryptobot.ValueObjects;

import eu.verdelhan.ta4j.Decimal;

public class TradingOrderVO {

    private Decimal quantity;
    private Decimal buyingPrice;
    private Decimal sellingPrice;
    private Decimal change;
    private Decimal changePercentage;

    public Decimal getQuantity() {
        return quantity;
    }

    public void setQuantity(Decimal quantity) {
        this.quantity = quantity;
    }

    public Decimal getBuyingPrice() {
        return buyingPrice;
    }

    public void setBuyingPrice(Decimal buyingPrice) {
        this.buyingPrice = buyingPrice;
    }

    public Decimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(Decimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public Decimal getChange() {
        return change;
    }

    public void setChange(Decimal change) {
        this.change = change;
    }

    public Decimal getChangePercentage() {
        return changePercentage;
    }

    public void setChangePercentage(Decimal changePercentage) {
        this.changePercentage = changePercentage;
    }
}
